import asyncio
import ccxt.async_support as ccxt
import ccxt as ccxt_e
import numpy as np
import math
import threading
import datetime
import time

class IntelligentOrderMaker(object):
    """
    Place market order on small amount over a period of time for prevent slipage.
    """

    def __init__(self, asyncio_loop, ccxt, bridge, max_order_amount):
        self.asyncio_loop = asyncio_loop
        self.ccxt = ccxt
        self.order_pool = {}
        self.executing_thread = None
        self.max_order_amount = max_order_amount
        self.bridge = bridge

    def set_logger(self, logger):
        self.logger = logger

    def make_order(self, symbol, side, asset_amount, close_intention=False):
        if symbol in self.order_pool :
            raise Exception("{} is in order list please wait until the order is completed \
                        before create new order.".format(symbol))
            return None
        asset_price = self.bridge.get_asset_price(symbol)
        asset_splited_amount = self.max_order_amount / asset_price
        self.order_pool[symbol] = {
            'side': side,
            'amount': asset_amount,
            'splited_amount': asset_splited_amount,
            'traded_amount' : 0,
            'execution_completed' : False,
            'close_intention' : close_intention,
        }

    def execute_order(self):
        if self.executing_thread is None or not self.executing_thread.isAlive() :
            self.executing_thread = threading.Thread(target=self._execute_order)
            self.executing_thread.start()
        else :
            pass
            # Note : Do nothings if execute_order not finish wait for next candle
            # raise Exception("Execute Order Fail, previous order execution don't finish yet.")

    def _execute_order(self):
        self.logger.info("Execute Order Started.")
        execute_finished = False
        while not execute_finished :
            execute_finished = True
            for symbol, order in self.order_pool.items():
                if not order['execution_completed'] :
                    last_trade = self._place_order(symbol, order)
                    if not last_trade :
                        execute_finished = False
                    time.sleep( 1 )
            self.bridge.repay_every_assets()
            time.sleep( 20 )
        self.order_pool = {}
        self.logger.info("Execute Order Completed.")

    def _place_order(self, symbol, order):
        print(symbol, order)
        type = 'market'
        params = { }
        order_success = False
        retry = 0
        if order['splited_amount'] * 2 > order['amount'] - order['traded_amount'] :
            amount = order['amount'] - order['traded_amount']
            last_trade = True
            '''
            # ToDo : for future, if amount matched position amount, close position for completely CLOSE.
            if self.bridge.default_type == 'future' :
                asset_amount = self.bridge.get_asset_amount(symbol.split('/')[0])
                if abs(asset_amount - amount) < self.bridge.fraction_amount :
                    params['closePosition'] = True
            '''
            # ToDo : if last_trade and order is intent to close, readjust position_amount to close it !!!!
            if order['close_intention'] :
                asset_amount = abs( self.bridge.get_asset_amount(symbol.split('/')[0]) )
                amount = asset_amount
        else :
            amount = order['splited_amount']
            last_trade = False
        while not order_success and retry < 3 :
            try :
                order['order'] = self.asyncio_loop.run_until_complete( self.ccxt.create_order(symbol,
                            type, order['side'], amount, params=params) )
                order_success = True
                order['traded_amount'] += order['order']['amount']
                order['execution_completed'] = last_trade
                self.logger.info("Place Order Success {} : {}".format(symbol, order['order']['info']))
                return last_trade
            except ccxt.ExchangeError as e :
                self.logger.info("Place Order ExchangeError {} : {}".format(symbol, e))
                print("Place Order ExchangeError {} : {}".format(symbol, e))
            except ccxt.NetworkError as e :
                self.logger.info("Place Order NetworkError {} : {}".format(symbol, e))
                print("Place Order NetworkError {} : {}".format(symbol, e))
            retry += 1
            time.sleep( 1 )
        self.logger.info("Critical Error {}".format(symbol))
        return True     # Preventing infinity loop by consider error lot as completed

class CCXTBinanceMarginBridge(object):

    def __init__(self, api_key, api_secret, max_order_amount=0.05, fraction_amount=0.0001, default_type='margin' ):
        self.asyncio_loop = asyncio.get_event_loop()
        self.exchange = ccxt.binance({
            'asyncio_loop': self.asyncio_loop,
            'enableRateLimit': True,
            'apiKey': api_key,
            'secret': api_secret,
            'options': { 'defaultType': default_type, 'adjustForTimeDifference': True},
            'verbose': False,

        })
        self.default_type = default_type
        self.fraction_amount = fraction_amount
        self.max_order_amount = max_order_amount
        self.order_maker = IntelligentOrderMaker( self.asyncio_loop, self.exchange, self, max_order_amount )

    def set_logger(self, logger):
        self.logger = logger
        self.order_maker.set_logger(logger)

    def set_leveraage(self, symbol, leverage):
        symbol = symbol.replace('/', '')
        if self.default_type == 'future' :
            params = { 'symbol': symbol, 'leverage': leverage }
            result = self.asyncio_loop.run_until_complete(self.exchange.fetch2('leverage',
                        api='fapiPrivate', method='POST', params=params))

    def get_tradable_amount(self):
        balance = self.asyncio_loop.run_until_complete( self.exchange.fetch_balance() )
        if self.default_type == 'margin' :
            return float( balance['info']['totalNetAssetOfBtc'] )
        elif self.default_type == 'future' :
            btc_price = self.get_asset_price('BTC/USDT')
            return float( balance['total']['USDT'] ) / btc_price

    def get_asset_amount(self, asset):
        if self.default_type == 'margin' :
            balance = self.asyncio_loop.run_until_complete( self.exchange.fetch_balance() )
            a = next( i for i in balance['info']['userAssets'] if i["asset"] == asset)
            return float( a['netAsset'] )
        elif self.default_type == 'future' :
            # ToDo : This code is for prevent error from exchange.
            # Note : Copy this code to every exchange call.
            completed = False
            while not completed :
                try :
                    balance = self.asyncio_loop.run_until_complete(
                                self.exchange.fetch2('positionRisk', 'fapiPrivate'))
                    completed = True
                except Exception as e :
                    self.logger.info("Get asset amount Error {} : {}".format(asset, e))
                    time.sleep(0.5)
            asset_amount = 0
            for a in balance :
                if a['symbol'] == asset + 'USDT' :
                    asset_amount += float( a['positionAmt'] )
            return asset_amount

    def get_asset_price(self, symbol):
        tick = self.asyncio_loop.run_until_complete( self.exchange.fetch_ticker(symbol) )
        return float( tick['info']['lastPrice'] )

    def repay(self, asset, amount):
        path = 'margin/repay'
        params = {
            'asset' : asset,
            'amount' : amount,
        }
        try :
            response = self.asyncio_loop.run_until_complete(
                    self.exchange.fetch2(path, 'sapi', 'post', params=params, headers=None, body=None) )
            return response
        except ccxt.ExchangeError as e :
            self.logger.info("Repay ExchangeError {} : {}".format(symbol, e))
        except ccxt.NetworkError as e :
            self.logger.info("Repay NetworkError {} : {}".format(symbol, e))
        return None

    def repay_every_assets(self):
        if self.default_type == 'margin' :
            balance = self.asyncio_loop.run_until_complete( self.exchange.fetch_balance() )
            for asset in balance['info']['userAssets'] :
                borrowed = float(asset['borrowed'])
                interest = float(asset['interest'])
                free = float(asset['free'])
                if borrowed + interest > 0 and free > 0 :
                    amount = min(borrowed + interest, free)
                    response = self.repay(asset['asset'], amount)
                time.sleep( 1 )

    def end_tick(self):
        self.order_maker.execute_order()

    def order_target_percent(self, symbol, target=0.0):
        if math.isclose(target, 0.0) :
            self.close(symbol)
        else :
            asset, base_asset = symbol.split('/')
            if base_asset == 'BTC' :
                possible_trade_amount = self.get_tradable_amount()
            elif base_asset == 'USDT' :
                btc_price = self.get_asset_price('BTC/USDT')
                possible_trade_amount = self.get_tradable_amount() * btc_price
            else :
                raise Exception("Base asset not support {}".format(base_asset))
            asset_amount = self.get_asset_amount(asset)
            asset_price = self.get_asset_price(symbol)
            asset_base_balance = asset_amount * asset_price
            c_percent = asset_base_balance / possible_trade_amount
            position_percent = target - c_percent
            position_base_amount = position_percent * possible_trade_amount
            p_asset_amount = abs( position_base_amount / asset_price )
            self.logger.info("[{}] Position Percent {}, Base Amount {}, Asset Amount {},".format(symbol,
                        position_percent, position_base_amount, p_asset_amount) )
            if abs(position_base_amount) < self.fraction_amount :
                return None
            if target > c_percent :             # long more or short less
                side = 'buy'
            else :                              # long less or short more
                side = 'sell'
            self.order_maker.make_order(symbol, side, p_asset_amount)

    # ToDo : if close short, calculate borrowed amount with interest, so we have less fraction amount.
    #        if possible close with positive fraction instead of negative fraction.
    def close(self, symbol):
        asset, base_asset = symbol.split('/')
        asset_amount = self.get_asset_amount(asset)
        asset_price = self.get_asset_price(symbol)
        asset_base_balance = asset_amount * asset_price
        if abs(asset_base_balance) < self.fraction_amount :
            return None
        if asset_base_balance > 0 :                  # close long
            side = 'sell'
        else :                                      # close short
            side = 'buy'
        print("Execute Close with : symbol {}, side {}, amount {}".format(symbol, side, abs(asset_amount)))
        self.order_maker.make_order(symbol, side, abs(asset_amount), close_intention=True)

    def current_position(self):
        balance = self.asyncio_loop.run_until_complete(
                                self.exchange.fetch2('positionRisk', 'fapiPrivate'))
        balance = [ {'symbol' : a['symbol'], 'amount' : float(a['positionAmt'])} for a in balance if float(a['positionAmt']) ]
        return balance

    def income_history(self, asset, start_date, end_date):
        st = time.mktime(start_date.timetuple())
        et = time.mktime(end_date.timetuple())
        params = { 'startTime': int(st)*1000, 'endTime': int(et)*1000, 'limit': 1000 }
        income = []
        result = self.asyncio_loop.run_until_complete( self.exchange.fetch2('income', 'fapiPrivate', params=params) )
        while len(result) > 0 :
            income += result
            params['startTime'] = int( income[-1]["time"] )
            result = self.asyncio_loop.run_until_complete( self.exchange.fetch2('income', 'fapiPrivate', params=params) )
            result = [ r for r in result if not r in income ]
        return_result = []
        for i in income :
            item = {
                'time': datetime.datetime.fromtimestamp(i['time'] / 1000).strftime('%Y-%m-%d %H:%M:%S'),
                'income': i['income'],
                'incomeType': i['incomeType'],
                'symbol': i['symbol'],
            }
            if (asset is not None and i['symbol'] == asset.replace('/', '')) or asset is None :
                return_result.append( item )
        return return_result

    def order_history(self, asset, start_date, end_date):
        st = time.mktime(start_date.timetuple())
        et = time.mktime(end_date.timetuple())
        until = int(et)*1000
        if asset is None :
            result = self.asyncio_loop.run_until_complete( self.exchange.load_markets() )
            asset_list = [r for r in result]
        else :
            asset_list = [asset]
        return_result = []
        for a in asset_list :
            history = []
            since = int(st)*1000
            result = self.asyncio_loop.run_until_complete( self.exchange.fetchOrders(a, since, 1000) )
            while len(result) > 0 :
                history += result 
                result = self.asyncio_loop.run_until_complete( self.exchange.fetchOrders(a, since, 1000) )
                result = [ r for r in result if not r in history ]
                time.sleep(0.1)
            for h in history :
                if h['info']['time'] < until :
                    i = {
                        'time' : datetime.datetime.fromtimestamp(h['info']['time'] / 1000).strftime('%Y-%m-%d %H:%M:%S'),
                        'symbol' : h['symbol'],
                        'filled' : h['filled'],
                        'side' : h['side'],
                        'cost' : h['cost'],
                        'average' : h['average'],
                        'status' : h['status'],
                    }
                    return_result.append( i )
        return return_result

# Chet future API KEY
# API_KEY = 'h6eipseKv5WVv0wdudqIbOj7nDAP1eXWUNbfvPXawiV6sjMGlSiHxU0AlhSBjfhq'
# API_SECRET = 'MdHsrgqtFI8MrGpBYcfuPRXDw7MvcGCwypVmUO8TuFLv6c9EQex1p72I4QdKszPl'
