import argparse
import logging
import sys
import pprint
import datetime
import csv

from ccxt_bridge import CCXTBinanceMarginBridge

def initial():
    parser = argparse.ArgumentParser(description='basic trading using api on binance exchange')
    parser.add_argument('command', metavar='C', type=str, nargs=1, 
                help="command : buy, sell, close, current_position, income_history, order_history")
    parser.add_argument('api_key', metavar='AK', type=str, nargs=1, help="api key")
    parser.add_argument('api_secret', metavar='AS', type=str, nargs=1, help="api secret")
    parser.add_argument('--asset', '-ast', type=str, help="asset")
    parser.add_argument('--amount', '-amt', type=float, help="asset amount.")
    parser.add_argument('--start_date', '-sd', type=datetime.date.fromisoformat, 
                help='Start date to get from history. default is 30 days eariler. date format as YYYY-MM-DD', 
                default=datetime.date.today())
    parser.add_argument('--end_date', '-ed', type=datetime.date.fromisoformat, 
                help='End date to get from history. default is today. date format as YYYY-MM-DD', 
                default=datetime.date.today() - datetime.timedelta(days=30))
    parser.add_argument('--file', '-f', type=str, help="output filename in the format of csv.", default=None)
    args = parser.parse_args()
    return args

def main(args):
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    logger = logging.getLogger()
    logger.addHandler(handler)
    ccxt_bridge = CCXTBinanceMarginBridge(args.api_key[0], args.api_secret[0],
            max_order_amount=300, fraction_amount=10, default_type='future')
    ccxt_bridge.set_logger( logger )
    if args.command[0] == 'buy' :
        ccxt_bridge.order_maker.make_order(args.asset, 'buy', args.amount)
        ccxt_bridge.end_tick()
    elif args.command[0] == 'sell' :
        ccxt_bridge.order_maker.make_order(args.asset, 'sell', args.amount)
        ccxt_bridge.end_tick()
    elif args.command[0] == 'close' :
        ccxt_bridge.close(args.asset)
        ccxt_bridge.end_tick()
    elif args.command[0] == 'current_position' :
        current_position = ccxt_bridge.current_position()
        pprint.pprint(current_position)
    elif args.command[0] == 'income_history' :
        history = ccxt_bridge.income_history(args.asset, args.start_date, args.end_date)
        if args.file is None :
            pprint.pprint(history) 
        else :
            with open(args.file, 'w') as csvfile:
                fieldnames = ['time', 'income', 'incomeType', 'symbol']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for h in history :
                    writer.writerow( h )
    elif args.command[0] == 'order_history' :
        history = ccxt_bridge.order_history(args.asset, args.start_date, args.end_date) 
        if args.file is None :
            pprint.pprint(history) 
        else :
            with open(args.file, 'w') as csvfile:
                fieldnames = ['time', 'symbol', 'filled', 'side', 'cost', 'average', 'status']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for h in history :
                    writer.writerow( h )

if __name__ == '__main__' :
    args = initial()
    main(args)

# Chet future API KEY
# API_KEY = 'h6eipseKv5WVv0wdudqIbOj7nDAP1eXWUNbfvPXawiV6sjMGlSiHxU0AlhSBjfhq'
# API_SECRET = 'MdHsrgqtFI8MrGpBYcfuPRXDw7MvcGCwypVmUO8TuFLv6c9EQex1p72I4QdKszPl'